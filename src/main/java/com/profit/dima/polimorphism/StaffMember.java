package com.profit.dima.polimorphism;

/**
 * Created by dima on 6/17/17.
 */
abstract class StaffMember {

    /**
     *
     */
    private String name;
    /**
     *
     */
    private String addres;

    /**
     *
     */
    private String phone;

    /**
     * Sets up staff member using the specifed information;
     *
     * @param eName
     * @param eAddres
     * @param ePhone
     */

    protected StaffMember(String eName, String eAddres, String ePhone) {
        name = eName;
        addres = eAddres;
        phone = ePhone;
    }

    /**
     * Returns a string including the basic employee information;
     *
     * @return
     */

    @Override
    public String toString() {
        String result = "Name" + name + "\n";

        result += "Addres: " + addres + "\n";
        result += "Phone: " + phone;
        return result;
    }

    /**
     * Derived classes must define the pay method for each type of employee;
     *
     * @return
     */
    public abstract double pay();
}
