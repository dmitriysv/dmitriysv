package com.profit.dima.polimorphism;

/**
 * Created by dima on 6/17/17.
 */
public class Hourly extends Employee {

    /**
     *
     */

    private int hoursWorked;

    /**
     * Set up an Employee with the specifided information.
     *
     * @param eName
     * @param eAddres
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Hourly(String eName, String eAddres, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddres, ePhone, socSecNumber, rate);
        hoursWorked = 0;
    }

    /**
     * @param moreHours
     */

    public void addHours(int moreHours) {
        hoursWorked += moreHours;
    }

    /**
     * @return
     */

    @Override
    public double pay() {
        double payment = payRate * hoursWorked;
        hoursWorked = 0;
        return payment;
    }

    /**
     * @return
     */

    @Override
    public String toString() {

        String result = super.toString();
        result += "\nnCurrent hours: " + hoursWorked;
        return result;
    }
}
