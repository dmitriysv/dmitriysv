package com.profit.dima.polimorphism;

/**
 * Created by dima on 6/17/17.
 */
public class Volunteer extends StaffMember {

    /**
     * @param eName
     * @param eAddres
     * @param ePhone
     */

    public Volunteer(String eName, String eAddres, String ePhone) {
        super(eName, eAddres, ePhone);
    }

    /**
     * @return
     */

    @Override
    public double pay() {
        return 0.0;
    }
}
