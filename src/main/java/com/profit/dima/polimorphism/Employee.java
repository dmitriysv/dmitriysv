package com.profit.dima.polimorphism;

/**
 * Created by dima on 6/17/17.
 */
public class Employee extends StaffMember {

    /**
     *
     */

    protected String socilaSecurityNumber;

    /**
     *
     */
    protected double payRate;

    /**
     * Set up an Employee with the specifided information.
     *
     * @param eName
     * @param eAddres
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */

    public Employee(String eName, String eAddres, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddres, ePhone);
        socilaSecurityNumber = socSecNumber;
        payRate = rate;
    }

    /**
     * Return information about an employee as a string;
     *
     * @return
     */

    @Override
    public String toString() {
        String result = super.toString();
        result += "\nSocial Security Number: " + socilaSecurityNumber;
        return result;

    }

    /**
     * Return the pay rate fot this employee;
     *
     * @return
     */

    @Override
    public double pay() {
        return payRate;

    }
}

