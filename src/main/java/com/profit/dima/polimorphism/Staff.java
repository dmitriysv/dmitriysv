package com.profit.dima.polimorphism;

import java.util.ArrayList;

/**
 * Created by dima on 6/17/17.
 */
public class Staff {

    /**
     *
     */

    private ArrayList<StaffMember> staffList;

    /**
     *
     */

    public Staff() {
        staffList = new ArrayList<>();
        staffList.add(new Executive("Sam", "123 Nfaf mmfmm", "555-12414", "1214124141", 3424.34));
        staffList.add(new Employee("Carla", "555 sfs mmfsffmm", "333-55656", "111111111", 5342.00));
        staffList.add(new Employee("Woody", "111 sfdffs", "1113-55656", "6465464564", 4500.00));
        staffList.add(new Hourly("Diane", "77 rtrtrt", "6545-11111", "6674745654", 2342.00));
        staffList.add(new Volunteer("Alan", "7127 yyttttt", "6545-11111"));
        staffList.add(new Volunteer("Cliff", "5433 uuuuuuuu", "5784-7777"));

        ((Executive) staffList.get(0)).awardBous(500.00);
        ((Hourly) staffList.get(3)).addHours(40);
    }

    /**
     *
     */

    public void payday() {
        double amount;
        for (int count = 0; count < staffList.size(); count++) {
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();

            if (amount == 0.0) {
                System.out.println("THanks!");
            } else {
                System.out.println("Paid: " + amount);
            }
            System.out.println(" ------------------------");

        }
    }
}
