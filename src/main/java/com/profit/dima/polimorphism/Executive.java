package com.profit.dima.polimorphism;

/**
 * Created by dima on 6/17/17.
 */
public class Executive extends Employee {

    /**
     *
     */

    private double bonus;

    /**
     * Set up an Employee with the specifided information.
     *
     * @param eName
     * @param eAddres
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Executive(String eName, String eAddres, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddres, ePhone, socSecNumber, rate);
        bonus = 0;
    }

    /**
     * @param execBonus
     */

    public void awardBous(double execBonus) {
        bonus = execBonus;
    }

    /**
     * Computes  and return the pay for an executive;
     *
     * @return
     */

    @Override
    public double pay() {
        double payment = super.pay() + bonus;
        bonus = 0;
        return payment;
    }
}
