package com.profit.dima.homework;


/**
 * Created by dima on 5/30/17.
 */
public class HomeworkDima {

    public HomeworkDima() {

    }

    /**
     * @param a
     * @param b
     */

    public static void maxOfTwoNumbers(Integer a, Integer b) {

        int firstNum = a;
        int secondNum = b;

        if (firstNum > secondNum) {

            System.out.println(secondNum);

        } else {

            System.out.println(secondNum);
        }

    }

    /**
     * @param a
     * @param b
     * @param c
     */

    public static void maxOfThreeNumbers(Integer a, Integer b, Integer c) {

        int firstNum = a;
        int secondNum = b;
        int thirdNum = c;


        if (firstNum > secondNum && firstNum > thirdNum) {
            System.out.println(firstNum);
        } else if (secondNum > firstNum && secondNum > thirdNum) {
            System.out.println(secondNum);
        } else if (thirdNum > firstNum && thirdNum > secondNum) {
            System.out.println(thirdNum);
        } else {
            System.out.println("ERROR! - Dublicate numbers!");

        }

    }

    /**
     *
     * @param a
     * @param b
     * @param c
     * @param d
     */

    public static void maxOfFourNumbers(Integer a, Integer b, Integer c, Integer d) {

        int firstNum = a;
        int secondNum = b;
        int thirdNum = c;
        int fourthNum = d;


        if (firstNum > secondNum && firstNum > thirdNum && firstNum > fourthNum) {
            System.out.println(firstNum);
        }
        if (secondNum > firstNum && secondNum > thirdNum && secondNum > fourthNum) {
            System.out.println(secondNum);
        }
        if (thirdNum > firstNum && thirdNum > secondNum && thirdNum > fourthNum) {
            System.out.println(thirdNum);
        }
        if (fourthNum > firstNum && fourthNum > secondNum && fourthNum > thirdNum) {
            System.out.println(fourthNum);
        } else {
            System.out.println("Pls do not dublicate numbers");
        }
    }

    /**
     * @param n
     * @param m
     */

    public static void evenNumOperation(Integer n, Integer m) {

        int number = n;
        int secNum = m;

        if (n % 2 == 0) {
            System.out.println(number * secNum);
        } else {
            System.out.println(number + secNum);
        }
    }

    /**
     *
     * @param tOne
     * @param tTwo
     * @param tThree
     * @param tFour
     */

    public static void sortingOperation(int tOne, int tTwo, int tThree, int tFour) {

        int numOne = tOne;
        int numTwo = tTwo;
        int numThree = tThree;
        int numFour = tFour;


        if (numOne > numTwo) {
            tTwo = numTwo;
            numTwo = numOne;
            numOne = tTwo;
        }
        if (numTwo > numThree) {
            tThree = numThree;
            numThree = numTwo;
            numTwo = tThree;
        }
        if (numThree > numFour) {
            tFour = numFour;
            numFour = numThree;
            numThree = tFour;
        }

        if (numOne > numTwo) {
            tTwo = numTwo;
            numTwo = numOne;
            numOne = tTwo;
        }
        if (numTwo > numThree) {
            tThree = numThree;
            numThree = numTwo;
            numTwo = tThree;
        }

        if (numOne > numTwo) {
            tTwo = numTwo;
            numTwo = numOne;
            numOne = tTwo;
        }
        System.out.println(numOne + "," + numTwo + "," + numThree + "," + numFour);

    }


}
