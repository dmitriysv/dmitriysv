package com.profit.dima.conditional;

/**
 * Created by dima on 5/28/17.
 */
public class ConditionalStatements {

    public ConditionalStatements() {
    }

    /**
     *
     * @param x
     */

    public static void positiveOrNegative(Integer x) {

        int input = x;

        if (input > 0) {
            System.out.println("Number is positive");
        } else if (input < 0) {
            System.out.println("Number is negative");
        } else {
            System.out.println("Number is zero");
        }
    }

}
