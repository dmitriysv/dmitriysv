package com.profit.dima.inheritance;

/**
 * Created by dima on 6/13/17.
 */
public class Piramide extends Triangle {

    public Piramide() {

    }

    /**
     *
     */

    private double height;

    public double getPiramideVolume() {
        return super.getAreaOfTriangle() * this.height;
    }

    @Override
    public String toString() {
        return "Piramide : subclass of : " + super.toString() + ", volume of piramide : " + getPiramideVolume();
    }
}
