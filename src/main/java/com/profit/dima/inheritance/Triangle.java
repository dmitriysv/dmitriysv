package com.profit.dima.inheritance;

/**
 * Created by dima on 6/12/17.
 */
public class Triangle extends Figure {

    /**
     *
     */

    private double firstSide;

    /**
     *
     */
    private double secondSide;

    /**
     *
     */
    private double thirdSide;

    public Triangle() {

    }

    public double getPerimeter() {

        return this.firstSide + this.secondSide + this.thirdSide;


    }

    public double getAreaOfTriangle() {
        return getPerimeter() / 2;
    }

    @Override
    public String toString() {
        return "Triangle: subclass of " + super.toString()
                + ", Perimeter : " + getPerimeter() + ", Area : " + getAreaOfTriangle();

    }

}
