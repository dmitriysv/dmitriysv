package com.profit.dima.inheritance;

/**
 * Created by dima on 6/11/17.
 */
public class Cube extends Square {

    /**
     *
     */

    private double height;

    public Cube(double height) {
        super();
        this.height = height;
    }

    public double getVolume() {
        return getSquareArea() * height;

    }

    public Cube() {

    }

    @Override
    public String toString() {
        return "Cube : subclass of " + super.toString() + " height :" + height + ", volume : " + getVolume();
    }
}
