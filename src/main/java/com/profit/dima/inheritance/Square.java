package com.profit.dima.inheritance;

/**
 * Created by dima on 6/11/17.
 */
public class Square extends Figure {

    /**
     *
     */

    private double weidth = 1.0;

    /**
     *
     */
    private double length = 1.0;

    public Square() {

    }

    public Square(double weidth, double length) {
        this.weidth = weidth;
        this.length = length;
    }

    public double getSquareArea() {
        return squareArea();
    }

    private double squareArea() {

        return this.weidth * this.length;
    }

    @Override
    public String toString() {
        return "Square : subclass of "
                + super.toString() + ", The area of square is " + squareArea();
    }


}