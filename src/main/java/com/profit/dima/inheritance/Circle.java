package com.profit.dima.inheritance;

/**
 * Created by dima on 6/11/17.
 */
public class Circle extends Figure {

    /**
     *
     */

    private double radius = 1.0;

    public double getRadius() {

        return radius;
    }

    public void setRadius(double radius) {

        this.radius = radius;
    }

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getArea() {

        return calculateArea();
    }

    private double calculateArea() {

        return this.radius * this.radius * Math.PI;
    }

    @Override
    public String toString() {
        return "The radius of the circle is : " + radius + ", and the area is : "
                + calculateArea();
    }
}


