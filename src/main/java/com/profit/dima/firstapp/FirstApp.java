package com.profit.dima.firstapp;

import static java.lang.System.*;

/**
 * Created by dima on 5/27/17.
 */
public class FirstApp {

    /**
     * print method
     * @param t
     * @param <T>
     */
    public static<T> void print(T t) {
        out.print(t);
    }

    /**
     * println method that prints Objects
     * @param object
     */
    public static void println(Object object) {
        out.print(object);
    }

    /**
     * println method that prints new line
     */

    public static void println() {
        out.println();
    }
}
