package com.profit.dima.loopsandarrays;

/**
 * Created by dima on 6/4/17.
 */
public class Sorting {

    /**
     * @param num
     */

    public static void selectionSort(int[] num) {

        int first;
        int temp;

        for (int i = num.length - 1; i > 0; i--) {

            // initialize to subscript of first element
            first = 0;

            // locate smallest element beetween positions 1 and i
            for (int j = 1; j <= i; j++) {

                if (num[j] < num[first]) {
                    first = j;
                }

            }

            //swap smallest found with element in position i
            temp = num[first];
            num[first] = num[i];
            num[i] = temp;

        }

    }
}
