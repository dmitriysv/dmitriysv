package com.profit.dima.loopsandarrays;

import static com.profit.dima.firstapp.FirstApp.println;

/**
 * Created by dima on 5/28/17.
 */
public class WhileLoops {

    /**
     *
     * @param str
     */

    public static void selectChoise(String str) {
        String choise;
        String con = "y";

        println("What is the command keyword to exit a loop in Java?");

        println("a.quit");
        println("b.continue");
        println("c.break");
        println("d.exit");

        while (con.compareTo("y") == 0) {

            println("Enter your choice:");

            choise = str;

            if (choise.compareTo("c") == 0) {

                println("Congratulation!");
                break;
            } else if (choise.compareTo("q") == 0
                    || choise.compareTo("e") == 0) {
                println("Exiting...!");
                break;
            } else {
                println("Incorrect!");

                break;
            }
        }
    }
}
