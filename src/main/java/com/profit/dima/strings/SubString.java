package com.profit.dima.strings;

/**
 * Created by dima on 6/10/17.
 */
public class SubString {

    // str = "abctrefcba", returns : "abc";
    // str = "rmmr", returns : "rm";
    // str = "asasfgh", returns : "";

    /**
     *
     * @param str
     * @return
     */

    public static String subStringSerach(String str) {
        StringBuilder result = new StringBuilder();

        int length = str.length();

        for (int i = 0; i < length / 2; i++) {
            char symbol = str.charAt(i);
            if (symbol == str.charAt(length - (i + 1))) {
                result.append(symbol);
            } else {
                break;
            }

        }
        return result.toString();
    }
}
