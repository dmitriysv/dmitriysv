package com.profit.dima.strings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by dima on 6/10/17.
 */


public class ReadFile {

    /**
     *
     * @param path
     */

    public static void readCsv(String path) {

        String line;

        try (BufferedReader rd = new BufferedReader(new FileReader(path))) {

            while ((line = rd.readLine()) != null) {
                System.out.println(line);

                StringTokenizer stringTokenizer = new StringTokenizer(line, "|");

                while (stringTokenizer.hasMoreElements()) {


                    Integer id = Integer.parseInt((stringTokenizer.nextElement().toString()));
                    Double price = Double.parseDouble((stringTokenizer.nextElement().toString()));
                    String userName = stringTokenizer.nextElement().toString();

                    StringBuilder sb = new StringBuilder();
                    sb.append("\nId : " + id);
                    sb.append("\nPrice : " + price);
                    sb.append("\nUserName : " + userName);
                    sb.append("\n*******************\n");

                    System.out.println(sb.toString());

                }
            }

            System.out.println("Done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
