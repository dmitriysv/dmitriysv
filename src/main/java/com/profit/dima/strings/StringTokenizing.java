package com.profit.dima.strings;

import java.util.StringTokenizer;

/**
 * Created by dima on 6/10/17.
 */
public class StringTokenizing {

    /**
     * @param str
     */

    public static void splitBySpace(String str) {

        StringTokenizer st = new StringTokenizer(str);

        System.out.println((" ----- Split By Space"));
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     * @param str
     */

    public static void splitByComma(String str) {
        System.out.println(" ----- Split By Comma ',' ------");
        StringTokenizer stTwo = new StringTokenizer(str, ",");

        while (stTwo.hasMoreElements()) {
            System.out.println(stTwo.nextElement());
        }
    }
}
