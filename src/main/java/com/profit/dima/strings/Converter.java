package com.profit.dima.strings;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**
 * Created by dima on 6/10/17.
 */
public class Converter {

    /**
     * @param xmlSource
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */


    public static Document stringToDom(String xmlSource)
            throws SAXException, ParserConfigurationException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.parse(new InputSource(new StringReader(xmlSource)));
    }

    /**
     * @param doc
     * @return
     * @throws TransformerException
     */


    public String documentToString(org.w3c.dom.Document doc)
            throws TransformerException {

        //Crate dom source for the document

        DOMSource domSource = new DOMSource(doc);

        // create a string writer
        StringWriter stringWriter = new StringWriter();

        //Create the result stream for the transform
        StreamResult result = new StreamResult(stringWriter);

        //Create a Transformer to serialize the document
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        transformer.setOutputProperty("indent", "");

        //Transform the document to the result stream
        transformer.transform(domSource, result);
        return stringWriter.toString();

    }

    /**
     * @param filename
     * @return
     */

    public static StringBuilder lineXml(String filename) {

        StringBuilder sb = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;


            while ((line = reader.readLine()) != null) {
                sb.append(line.trim());
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return sb;
    }

    /**
     * @param xmlSource
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerException
     */

    public static void stringToDomFile(String xmlSource) throws SAXException,
            ParserConfigurationException, IOException, TransformerException {

        //Parse the given document

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xmlSource)));

        //
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StreamResult result = new StreamResult(new File("my-file.xml"));
        transformer.transform(source, result);

    }
}


