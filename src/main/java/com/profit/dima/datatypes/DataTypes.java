package com.profit.dima.datatypes;

/**
 * Created by dima on 5/28/17.
 */
public class DataTypes {

    /**
     *
     */

    private byte aByte;

    /**
     *
     */
    private short aShort;

    /**
     *
     */
    private int anInt;

    /**
     *
     */
    private long aLong;

    /**
     *
     */
    private char aChar;

    /**
     *
     */
    private boolean aBoolean;

    /**
     *
     */
    private float aFloat;

    /**
     *
     */
    private double aDouble;

    /**
     *
     * @return
     */

    public byte getaByte() {
        return aByte;
    }

    /**
     *
     * @param aByte
     */

    public void setaByte(byte aByte) {
        this.aByte = aByte;
    }

    /**
     *
     * @return
     */

    public short getaShort() {
        return aShort;
    }

    /**
     *
     * @param aShort
     */

    public void setaShort(short aShort) {
        this.aShort = aShort;
    }

    /**
     *
     * @return
     */

    public int getAnInt() {
        return anInt;
    }

    /**
     *
     * @param anInt
     */

    public void setAnInt(int anInt) {
        this.anInt = anInt;
    }

    /**
     *
     * @return
     */

    public long getaLong() {
        return aLong;
    }

    /**
     *
     * @param aLong
     */

    public void setaLong(long aLong) {
        this.aLong = aLong;
    }

    /**
     *
     * @return
     */

    public char getaChar() {
        return aChar;
    }

    /**
     *
     * @param aChar
     */

    public void setaChar(char aChar) {
        this.aChar = aChar;
    }

    /**
     *
     * @return
     */

    public boolean isaBoolean() {
        return aBoolean;
    }

    /**
     *
     * @param aBoolean
     */

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    /**
     *
     * @return
     */

    public float getaFloat() {
        return aFloat;
    }

    /**
     *
     * @param aFloat
     */

    public void setaFloat(float aFloat) {
        this.aFloat = aFloat;
    }

    /**
     *
     * @return
     */

    public double getaDouble() {
        return aDouble;
    }

    /**
     *
     * @param aDouble
     */

    public void setaDouble(double aDouble) {
        this.aDouble = aDouble;
    }


    public DataTypes(byte aByte, short aShort, int anInt, long aLong, char aChar, boolean aBoolean,
                     float aFloat, double aDouble) {
        this.aByte = aByte;
        this.aShort = aShort;
        this.anInt = anInt;
        this.aLong = aLong;
        this.aChar = aChar;
        this.aBoolean = aBoolean;
        this.aFloat = aFloat;
        this.aDouble = aDouble;
    }
}
