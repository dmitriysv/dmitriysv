package com.profit.dima.incapsulation;

import java.util.Date;

/**
 * Created by dima on 6/17/17.
 */
public class User {
    /**
     *
     */
    private String firstname;
    /**
     *
     */
    private String lastName;
    /**
     *
     */
    private String longID;
    /**
     *
     */
    private String password;
    /**
     *
     */
    private String email;
    /**
     *
     */
    private Date registered;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLongID() {
        return longID;
    }
    /**
     public void setLongID(String longID) {
     this.longID = longID;
     }
     */

    public void setLongID(String longID) {
        if (longID.matches("^[a-zA-Z][a-zA-Z0-9]*$")) {
            this.longID = longID;
        } else {
            System.out.println();
        }
    }

    public String getPassword() {
        return password;
    }
    /**
     public void setPassword(String password) {
     if (password == null || password.length() < 6) {
     System.err.println("Password is too short ");
     } else {
     this.password = password;
     }
     }

     */


    public void setPassword(String password) {
        if (password.length() < 6) {
            System.out.println("Password is too short ");
        } else {
            this.password = password;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }
}


