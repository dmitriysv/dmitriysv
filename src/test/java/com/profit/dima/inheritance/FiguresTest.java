package com.profit.dima.inheritance;

import com.sun.xml.internal.bind.CycleRecoverable;
import org.junit.Test;

import javax.xml.transform.stream.StreamResult;
import java.security.cert.CertificateNotYetValidException;

/**
 * Created by dima on 6/11/17.
 */
public class FiguresTest {

    @Test
    public void getCircleData() {

        Circle circle = new Circle();
        System.out.println(circle.toString());

        Circle circle1 = new Circle(4.2);
        System.out.println(circle1.toString());


    }

    @Test
    public void cylinderData() {

        System.out.println("-------------Cylinder 1-------------");

        Cylinder cylinder = new Cylinder();
        System.out.println(cylinder.getHeight());
        System.out.println(cylinder.getRadius());
        System.out.println(cylinder.getArea());
        System.out.println(cylinder.getVolume());
        System.out.println(cylinder.toString());

        System.out.println("-------------Cylinder 2-------------");

        Cylinder cylinder1 = new Cylinder(3.4);
        System.out.println(cylinder1.getHeight());
        System.out.println(cylinder1.getVolume());
        System.out.println(cylinder1.toString());

        System.out.println("-------------Cylinder 3-------------");

        Cylinder cylinder2 = new Cylinder(4.5, 6.7);
        System.out.println(cylinder2.getRadius());
        System.out.println(cylinder2.getHeight());
        System.out.println(cylinder2.getArea());
        System.out.println(cylinder2.getVolume());
        System.out.println(cylinder2.toString());



    }

    @Test
    public void cubeData(){

        Cube cube = new Cube(4);

        System.out.println("Cube volume : " + cube.getVolume());
    }

    @Test
    public void squareData(){

        Square square = new Square();
        System.out.println(square.getSquareArea());
        System.out.println(square.toString());
        Square square1 = new Square(2.3, 4.3);
        System.out.println(square1.toString());


    }

    @Test
    public void triangleData(){
        Triangle triangle = new Triangle();
        System.out.println(triangle.toString());
    }

    @Test
    public void piramideData(){
        Piramide piramide = new Piramide();
        System.out.println(piramide.toString());
    }


}

