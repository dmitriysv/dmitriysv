package com.profit.dima.homework;

import org.junit.Test;

import static com.profit.dima.homework.HomeworkDima.*;

/**
 * Created by dima on 5/31/17.
 */
public class HomeworkDimaTesting {

    @Test
    public void maxOfTwoNumbersTesting() {

        maxOfTwoNumbers(5, 7);
    }

    @Test
    public void maxOfThreeNumbersTesting() {

        maxOfThreeNumbers(10, 10, 8);

    }

    @Test
    public void maxOfFourNumbersTesting() {

       maxOfFourNumbers(9, 9, 5, 1);
    }

    @Test
    public void evenNumOperationTesting() {

        evenNumOperation( -2, 3);
    }

    @Test
    public void sortingOperationsTesting() {

        sortingOperation(2, 72, 4, 10);
    }
}
