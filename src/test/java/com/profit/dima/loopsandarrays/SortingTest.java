package com.profit.dima.loopsandarrays;

import org.junit.Test;

import java.util.Arrays;


/**
 * Created by dima on 6/4/17.
 */
public class SortingTest {

    @Test

    public void selectionSortingTesting() {

        int [] mas = {5, 4, 12, 8};

        Sorting.selectionSort(mas);

        System.out.println(Arrays.toString(mas));

    }

}
