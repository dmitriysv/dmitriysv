package com.profit.dima.strings;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Created by dima on 6/11/17.
 */
public class ParserTesting {

    @Test
    public void parser() throws IOException, SAXException,
            ParserConfigurationException, TransformerException {
        String x = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                "<ListDomainsResponse xmlns=\"http://sdb.amazonaws.com/doc/2009-04-15/\">\n" +
                        "    <ListDomainsResult>\n" +
                        "        <DomainName>Audio</DomainName>\n" +
                        "        <DomainName>Course</DomainName>\n" +
                        "        <DomainName>DocumentContents</DomainName>\n" +
                        "        <DomainName>LectureSet</DomainName>\n" +
                        "        <DomainName>MetaData</DomainName>\n" +
                        "        <DomainName>Professors</DomainName>\n" +
                        "        <DomainName>Tag</DomainName>\n" +
                        "    </ListDomainsResult>\n" +
                        "    <ResponseMetadata>\n" +
                        "        <RequestId>42330b4a-e134-6aec-e62a-5869ac2b4575</RequestId>\n" +
                        "        <BoxUsage>0.0000071759</BoxUsage>\n" +
                        "    </ResponseMetadata>\n" +
                        "</ListDomainsResponse>";

  //      Converter converter = new Converter();
   //     System.out.println(converter.documentToString(Converter.stringToDom(x)));

        Converter.stringToDomFile(x);
    }

    @Test
    public void readXml() throws IOException, ParserConfigurationException,SAXException {
        System.out.println(Converter.lineXml("my-file.xml"));
    }
}
