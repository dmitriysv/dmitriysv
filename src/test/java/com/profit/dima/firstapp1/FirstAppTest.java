package com.profit.dima.firstapp1;

import org.junit.Test;

import static com.profit.dima.firstapp.FirstApp.print;
import static com.profit.dima.firstapp.FirstApp.println;

/**
 * Created by dima on 5/27/17.
 */
public class FirstAppTest {
    @Test
    public void firstAppTesting() {
        print("Hello");
        println();
        println("Profit");

    }
}
