package com.profit.dima.incapsulation;

import org.junit.Test;

import javax.jws.soap.SOAPBinding;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by dima on 6/17/17.
 */
public class UserTest {

    @Test
    public void setPasswordNormal() {
        User u = new User();
        u.setPassword("qwerty");
        assertEquals("qwerty", u.getPassword());
    }

    @Test
    public void setPasswordShort() {
        User u = new User();
        u.setPassword("aa");
        assertEquals(null, u.getPassword());
    }

    @Test
    public void setPasswordNull() {
        User u = new User();
        u.setPassword("qq");
        assertEquals(null, u.getPassword());

    }

    @Test (expected = NullPointerException.class)
    public void setPasswordNull2() {
        User u = new User();
        u.setPassword(null);
        assertEquals(null, u.getPassword());
    }

//    @Test
//    public void setLoginIdValid() {
//        User u = new User();
//        u.setLongID("123456");
//        assertEquals("123456", u.getLongID());
//    }

    @Test
    public void setLoginIdValid2() {
        User u = new User();
        u.setPassword("@#@#@#&");
        assertEquals(null, u.getLongID());
    }

}

